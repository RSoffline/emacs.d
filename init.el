;;set tab width
(setq-default tab-width 4 indent-tabs-mode nil)
(setq auto-newline t)
;;display column number
(column-number-mode t)
;;deisplay line number
(global-linum-mode t)
;;highlight parenthesis
(show-paren-mode 1)
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq make-backup-files nil)
(setq auto-save-default nil)
;;set PATH
(load-file (expand-file-name "~/.emacs.d/shellenv.el"))
(dolist (path (reverse (split-string (getenv "PATH") ":")))
    (add-to-list 'exec-path path))
;;window is split and eshell started when emacs is started
(add-hook 'after-init-hook (lambda()
    (setq w (selected-window))
    (setq w2 (split-window w (- (window-height w) 4)))
    (select-window w2)
    (eshell)
    (select-window w)))
;;window move
(windmove-default-keybindings 'meta)
(set-frame-parameter nil 'alpha 90)

;;insert template when a new file is made
(require 'autoinsert)
(setq auto-insert-directory "//.emacs.d/insert/")

(setq auto-insert-alist
      (nconc '(
               ("\\.py$"  .["template.py" my-template])
               ("\\.cpp$" .["template.cpp" my-template])
               ) auto-insert-alist))
(require 'cl)

(defvar template-replacements-alists
  '(("%file%".(lambda () (file-name-nondirectory (buffer-file-name))))
    ("%file-without-ext%" .(lambda () (file-name-sans-extension (file-name-nondirectory (buffer-file-name)))))
    ("%include-guard%" .(lambda () (format "__SCHEME_%s__" (upcase (file-name-sans-extension (file-name-nondirectory buffer-file-name))))))))

(defun my-template ()
  (time-stamp)
  (mapc #'(lambda(c)
            (progn
              (goto-char (point-min))
              (replace-string (car c) (funcall (cdr c)) nil)))
        template-replacements-alists)
  (goto-char (point-max))
  (message "done."))
(add-hook 'find-file-not-found-hooks 'auto-insert)

;;making scripts executable on save
(add-hook 'after-save-hook 'executable-make-buffer-file-if-script-p)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
